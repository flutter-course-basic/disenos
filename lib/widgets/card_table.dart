import 'dart:ui';

import 'package:flutter/material.dart';

class CardTable extends StatelessWidget {
  const CardTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(children: const [
      TableRow(children: [
        _SingleCard(
          color: Colors.blue,
          text: 'General',
          icon: Icons.pie_chart_sharp,
        ),
        _SingleCard(
          color: Colors.purple,
          text: 'Transport',
          icon: Icons.car_rental,
        )
      ]),
      TableRow(children: [
        _SingleCard(
          color: Colors.purpleAccent,
          text: 'Shop',
          icon: Icons.pie_chart_sharp,
        ),
        _SingleCard(
          color: Colors.green,
          text: 'Paper',
          icon: Icons.panorama_photosphere_select,
        )
      ]),
      TableRow(children: [
        _SingleCard(
          color: Colors.purpleAccent,
          text: 'Shop',
          icon: Icons.pie_chart_sharp,
        ),
        _SingleCard(
          color: Colors.green,
          text: 'Paper',
          icon: Icons.panorama_photosphere_select,
        )
      ]),
      TableRow(children: [
        _SingleCard(
          color: Colors.purpleAccent,
          text: 'Shop',
          icon: Icons.pie_chart_sharp,
        ),
        _SingleCard(
          color: Colors.green,
          text: 'Paper',
          icon: Icons.panorama_photosphere_select,
        )
      ])
    ]);
  }
}

class _SingleCard extends StatelessWidget {
  const _SingleCard(
      {Key? key, required this.icon, required this.text, required this.color})
      : super(key: key);

  final IconData icon;
  final String text;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return _CardBackground(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      CircleAvatar(
        backgroundColor: color,
        child: Icon(icon, color: Colors.white),
        radius: 30,
      ),
      const SizedBox(height: 10),
      Text(text, style: TextStyle(fontSize: 18, color: color)),
    ]));
  }
}

class _CardBackground extends StatelessWidget {
  const _CardBackground({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Container(
            height: 180,
            decoration: BoxDecoration(
                color: const Color.fromRGBO(62, 66, 107, 0.7),
                borderRadius: BorderRadius.circular(20)),
            child: child,
          ),
        ),
      ),
    );
  }
}
