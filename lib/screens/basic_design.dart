import 'package:flutter/material.dart';

class BasicDesignScreen extends StatelessWidget {
  const BasicDesignScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          // Imagen
          const Image(image: AssetImage('assets/landscape.jpg')),

          // Titulo
          const Title(),

          // Button Section
          const ButtonSection(),

          // Description
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: const Text(
                'Dolore eu culpa cupidatat nisi enim. Officia nulla cupidatat voluptate veniam aliquip et veniam consequat anim ut tempor ut ipsum. Esse velit et anim anim pariatur. Deserunt in eiusmod exercitation ex mollit do aute incididunt adipisicing culpa non occaecat sit culpa. Nostrud excepteur commodo exercitation labore est est aute id esse enim proident nostrud incididunt pariatur. Qui proident aliquip eiusmod nostrud. Reprehenderit officia fugiat irure irure ea ullamco tempor.'),
          )
        ],
      ),
    );
  }
}

class Title extends StatelessWidget {
  const Title({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Row(children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Text('Oeschinen Lake campground',
                style: TextStyle(fontWeight: FontWeight.bold)),
            Text('kandersteg, Switzerland ',
                style: TextStyle(color: Colors.black45))
          ],
        ),
        Expanded(child: Container()),
        const Icon(Icons.star, color: Colors.red),
        const Text('41'),
      ]),
    );
  }
}

class ButtonSection extends StatelessWidget {
  const ButtonSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const [
            CustomButton(icon: Icons.phone, text: 'CALL'),
            CustomButton(icon: Icons.route, text: 'ROUTE'),
            CustomButton(icon: Icons.share, text: 'SHARE'),
          ]),
    );
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key? key,
    required this.icon,
    required this.text,
  }) : super(key: key);

  final IconData icon;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(icon, color: Colors.blue),
        const SizedBox(height: 5),
        Text(text, style: const TextStyle(color: Colors.blue))
      ],
    );
  }
}
